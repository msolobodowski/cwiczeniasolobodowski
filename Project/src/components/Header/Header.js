/* eslint-env browser */
import $ from 'jquery';

const Header = {
  settings: {
    target: '.HeaderLine',
    item: ' .HeaderLine__dropdown-content',
    button: '.HeaderLine__mobileMenu',
    content: '.MainPageContent'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if (this.settings.target.length) {
      this.catchDOM(this.settings, this.afterInit.bind(this));
    }
  },
  catchDOM(settings, callback) {
    const target = $(settings.target);
    const item = $(settings.item);
    const button = $(settings.button);
    this.$target = {
      root: target,
      item,
      button
    };
    if (this.$target.root && this.$target.root.length > 0) callback();
  },
  bindEvents() {
    this.$target.button.on('click', this.toggleMenu.bind(this));
    $(document).on('click', this.closeMenu.bind(this));
  },

  afterInit() {
    this.bindEvents();
  },

  toggleMenu(event) {
    event.stopPropagation();
    $(this.$target.item).toggle('HeaderLine__show');
  },

  closeMenu() {
    $(this.$target.item).hide();
  }
};
export default Header;
