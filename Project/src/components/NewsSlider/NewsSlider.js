/* eslint-env browser */
import $ from 'jquery';
import 'slick-carousel';

const NewsSlider = {
  settings: {
    target: '.NewsSlider',
    box: '.NewsSlider__box',
    item: '.NewsSlider__slideNews',
    options: {
      arrows: true,
      prevArrow:
        '<i class="material-icons NewsSlider__prevNews">chevron_left</i>',
      nextArrow:
        '<i class="material-icons NewsSlider__nextNews">chevron_right</i>',
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if (this.settings.target.length) {
      this.catchDOM(this.settings, this.afterInit.bind(this));
    }
  },
  catchDOM(settings, callback) {
    const target = $(settings.target);
    const box = $(settings.box);
    const item = $(settings.item);
    this.$target = {
      root: target,
      box,
      item
    };
    if (this.$target.root && this.$target.root.length > 0) callback();
  },
  bindEvents() {},

  afterInit() {
    this.initSlick();
  },

  initSlick() {
    $(this.$target.box).slick(this.settings.options);
  }
};
export default NewsSlider;
