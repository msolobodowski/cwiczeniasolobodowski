/* eslint-env browser */
import $ from 'jquery';
import 'slick-carousel';

const ProducersSlider = {
  settings: {
    target: '.ProducersSlider',
    box: '.ProducersSlider__box',
    item: '.ProducersSlider__slide',
    options: {
      arrows: true,
      prevArrow:
        '<i class="material-icons ProducersSlider__prev">arrow_back_ios</i>',
      nextArrow:
        '<i class="material-icons ProducersSlider__next">arrow_forward_ios</i>',
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 520,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if (this.settings.target.length) {
      this.catchDOM(this.settings, this.afterInit.bind(this));
    }
  },
  catchDOM(settings, callback) {
    const target = $(settings.target);
    const box = $(settings.box);
    const item = $(settings.item);
    this.$target = {
      root: target,
      box,
      item
    };
    if (this.$target.root && this.$target.root.length > 0) callback();
  },
  bindEvents() {},

  afterInit() {
    this.initSlick();
  },

  initSlick() {
    $(this.$target.box).slick(this.settings.options);
  }
};
export default ProducersSlider;
