/* eslint-env browser */
import $ from 'jquery';
import 'slick-carousel';

const Events = {
  settings: {
    target: '.Events',
    box: '.Events__box',
    item: '.Events__slideNews',
    options: {
      arrows: true,
      prevArrow: '<i class="material-icons Events__prevNews">chevron_left</i>',
      nextArrow: '<i class="material-icons Events__nextNews">chevron_right</i>',
      dots: true,
      customPaging: function() {
        return '<i class="fas fa-square dots"></i>';
      },
      infinite: true,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1023,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if (this.settings.target.length) {
      this.catchDOM(this.settings, this.afterInit.bind(this));
    }
  },
  catchDOM(settings, callback) {
    const target = $(settings.target);
    const box = $(settings.box);
    const item = $(settings.item);
    this.$target = {
      root: target,
      box,
      item
    };
    if (this.$target.root && this.$target.root.length > 0) callback();
  },
  bindEvents() {},

  afterInit() {
    this.initSlick();
  },

  initSlick() {
    $(this.$target.box).slick(this.settings.options);
  }
};
export default Events;
