import 'babel-polyfill';
import $ from 'jquery';
import ProducersSlider from './components/ProducersSlider/ProducersSlider';
import EventsHeader from './components/EventsHeader/EventsHeader';
import Events from './components/Events/Events';
import MainPageOffer from './components/MainPageOffer/MainPageOffer';
import AboutSlider from './components/AboutSlider/AboutSlider';
import NewsSlider from './components/NewsSlider/NewsSlider';
import Header from './components/Header/Header';
$(document).ready(() => {
  Header.init();
  ProducersSlider.init();
  EventsHeader.init();
  Events.init();
  MainPageOffer.init();
  AboutSlider.init();
  NewsSlider.init();
});
